var Bicicleta = require('../models/bicicleta');

exports.Bicicleta_list = function (req, res) {
    Bicicleta.allBicis((err, bici) => {
        console.log(bici);
        res.render('bicicletas/index', {bicis : bici});
    });
}

exports.Bicicleta_create_get = function(req, res){
    res.render('bicicletas/create')
}

exports.Bicicleta_create_post = function(req, res) {
    
    
    //console.log(req.body);

    // var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo);
    // bici.ubicacion = [req.body.lat, req.body.lng];
    
    var aBici = {
        "code": req.body.id,
        "color": req.body.color,
        "modelo": req.body.modelo,
        "ubicacion": [req.body.lat,req.body.lng]
    };

    Bicicleta.add(aBici,(err) => {
        if (err) return console.log(err);
        console.log('bicicleta created')
        res.redirect('/bicicletas');
    });

}

exports.Bicicleta_update_get = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {bici})
}

exports.Bicicleta_update_post = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    
    res.redirect('/bicicletas');
}


exports.Bicicleta_delete_post = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}