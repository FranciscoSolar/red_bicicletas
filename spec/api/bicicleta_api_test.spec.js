var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


describe("Bicicleta API",()=>{

    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser:true});
        
        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            done();
         });
    });


    describe("GET Bicicleta", () =>{
        it("Status 200", (done)=>{
            request.get('http://localhost:5000/api/bicicletas',function(error,response,body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe("POST Bicicleta", () =>{
        it("Status 200", (done)=>{
            var headers = {'content-type':'application/json'};
            var aBici = '{"code":10,"color":"rojo","modelo":"urbana","lat":20,"lng":12}';
            request.post({
                headers:headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: aBici
                }, function(error,response,body){
                    expect(response.statusCode).toBe(200);
                    
                    var bici = JSON.parse(body);
                    //console.log(bici.ubicacion);
                    //console.log(bici);
                    expect(bici.color).toBe('rojo');
                    expect(bici.modelo).toBe('urbana');               
                    expect(bici.ubicacion[0]).toBe(20);          
                    expect(bici.ubicacion[1]).toBe(12);
                    done();
            });
        });
    });

    describe("DELETE Bicicleta", () =>{
        it("Status 204", (done)=>{
            var headers = {'content-type':'application/json'};
            var aBici = new Bicicleta({code:1,color:'verde',modelo:'urbana'});
            Bicicleta.add(aBici,function(err,newBici){
                
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toBe(1);    
                    if(err) console.log(err);
                    request.delete({
                        headers:headers,
                        url: 'http://localhost:5000/api/bicicletas/delete',
                        body: '{"code":1}'
                    }, function(error,response,body){
                        Bicicleta.allBicis(function(err, bicis){
                            expect(bicis.length).toBe(0);
                            expect(response.statusCode).toBe(204);
                            done();
                        });

                    });
                });
            });         
        });
    });

    describe("UPDATE Bicicleta", () =>{
        it("Status 200", (done)=>{
            var headers = {'content-type':'application/json'};
            var aBici = new Bicicleta({code:1,color:'verde',modelo:'urbana'});
            Bicicleta.add(aBici,function(err,newBici){
                Bicicleta.findByCode(1,function(err,bici){
                    console.log(bici._id);
                    body = '{"code":1,"color":"amarillo","modelo":"ürbana","lat":12,"lng":213,"_id":"' + bici._id +'"}'


                    request.put({
                        headers:headers,
                        url: 'http://localhost:5000/api/bicicletas/update',
                        body: body
                    }, function(error,response,body){
                        expect(response.statusCode).toBe(200);
                        done();
                    });
                });
                
            });         
        });
    }); 
});