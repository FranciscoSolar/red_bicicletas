var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var request = require('request');
var server = require('../../bin/www');


describe("Usuario API",()=>{
    describe("POST Usuario", () =>{
        it("Status 200", (done)=>{
            var headers = {'content-type':'application/json'};
            var aUser = '{"nombre":"María Apolo"}';
            request.post({
                headers:headers,
                url: 'http://localhost:5000/api/usuarios/create',
                body: aUser
                }, function(error,response,body){
                    expect(response.statusCode).toBe(200);
                    
                    var user = JSON.parse(body);
                    //console.log(bici.ubicacion);
                    //console.log(bici);
                    expect(user.nombre).toBe('María Apolo');
                    
                    done();
            });
        });
    });
});